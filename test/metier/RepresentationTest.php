<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Representation Test</title>
    </head>
    <body>
        <?php
        use modele\metier\Representation;
        use modele\metier\Groupe;
        use modele\metier\Lieux;
        
        require_once __DIR__ . '/../../includes/autoload.inc.php';
        echo "<h2>Test unitaire de la classe métier Representation</h2>";
        
        $id = ('01');
        $unGroupe = new Groupe("g999","les Joyeux Turlurons","général Alcazar","Tapiocapolis" ,25,"San Theodoros","N");
        $unLieu = new Lieux("01", "Saint Sebastien","50", "7 rue de la Joliverie");
        $date = ("07/07/2020");
        $hDeb = ("14:00");
        $hFin = ("16:00");
        
        $objet = new Representation($id, $unGroupe, $unLieu, $date, $hDeb, $hFin);
        var_dump($objet);
        ?>
    </body>
</html>

