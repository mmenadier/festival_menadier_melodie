<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Lieux Test</title>
    </head>
    <body>
        <?php
        use modele\metier\Lieux;
        require_once __DIR__ . '/../../includes/autoload.inc.php';
        echo "<h2>Test unitaire de la classe métier Lieux</h2>";
        
        $objet =  $unLieu = new Lieux("01", "Saint Sebastien","50", "7 rue de la Joliverie");
        var_dump($objet);
        ?>
    </body>
</html>