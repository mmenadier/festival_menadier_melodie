<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>RepresentationDAO : test</title>
    </head>

    <body>

        <?php

        use modele\metier\Representation;
        use modele\dao\RepresentationDAO;
        use modele\dao\GroupeDAO;
        use modele\metier\Groupe;
        use modele\dao\LieuxDAO;
        use modele\metier\Lieux;
        use modele\dao\Bdd;
        use controleur\Session;

require_once __DIR__ . '/../../includes/autoload.inc.php';

        $idRepresentation = '01';
        Session::demarrer();
        Bdd::connecter();

        echo "<h2>Test RepresentationDAO</h2>";

        // Test n°1
        echo "<h3>1- Test getOneById</h3>";
        try {
            $objet = RepresentationDAO::getOneById($idRepresentation);
            var_dump($objet);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }

        // Test n°2
        echo "<h3>2- Test getAll</h3>";
        try {
            $lesObjets = RepresentationDAO::getAll();
            var_dump($lesObjets);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }
/*
        // Test n°3
        echo "<h3>3- insert</h3>";
        try {
            $idRepresentation = '05';
            $objet = new Representation($idRepresentation, 'g002', '02', "2020-07-08", "13:00:00", "14:00:00");
            $ok = RepresentationDAO::insert($objet);
            if ($ok) {
                echo "<h4>ooo réussite de l'insertion ooo</h4>";
                $objetLu = RepresentationDAO::getOneById($idRepresentation);
                var_dump($objetLu);
            } else {
                echo "<h4>*** échec de l'insertion ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        }
        // Test n°3-bis
        echo "<h3>3-bis insert déjà présent</h3>";
        try {
            $idRepresentation = '01';
            $objet = new Representation($idRepresentation, 'g002', '02', "2020-07-08", "13:00:00", "14:00:00");
            $ok = RepresentationDAO::insert($objet);
            if ($ok) {
                echo "<h4>*** échec du test : l'insertion ne devrait pas réussir  ***</h4>";
                $objetLu = Bdd::getOneById($idRepresentation);
                var_dump($objetLu);
            } else {
                echo "<h4>ooo réussite du test : l'insertion a logiquement échoué ooo</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>ooo réussite du test : la requête d'insertion a logiquement échoué ooo</h4>" . $e->getMessage();
        }
        
        // Test n°4
        echo "<h3>4- update</h3>";
        try {
            $objet->setDate('2020-07-08');
            $objet->setHFin('14:30:00');
            $ok = RepresentationDAO::update($idRepresentation, $objet);
            if ($ok) {
                echo "<h4>ooo réussite de la mise à jour ooo</h4>";
                $objetLu = RepresentationDAO::getOneById($idRepresentation);
                var_dump($objetLu);
            } else {
                echo "<h4>*** échec de la mise à jour ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        }
*/
        // Test n°5
        echo "<h3>5- delete</h3>";
        try {
            $ok = RepresentationDAO::delete($idRepresentation);
//            $ok = RepresentationDAO::delete("xxx");
            if ($ok) {
                echo "<h4>ooo réussite de la suppression ooo</h4>";
            } else {
                echo "<h4>*** échec de la suppression ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        }
        
        // Test n°6
        echo "<h3>6- isAnExistingId</h3>";
        try {
            $idRepresentation = "02";
            $ok = RepresentationDAO::isAnExistingId($idRepresentation);
            $ok = $ok && !RepresentationDAO::isAnExistingId('AZERTY');
            if ($ok) {
                echo "<h4>ooo test réussi ooo</h4>";
            } else {
                echo "<h4>*** échec du test ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        }
        
        Bdd::deconnecter();
        Session::arreter();
        ?>


    </body>
</html>
