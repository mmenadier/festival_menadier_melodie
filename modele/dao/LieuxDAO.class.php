<?php
namespace modele\dao;

use modele\metier\Lieux;
use PDOStatement;
use PDO;

/**
 * Description of LieuDAO
 * Classe métier :  Lieu
 * @author melodie
 * @version 2020
 */
class LieuxDAO {


     /**
     * Instancier un objet de la classe Lieu à partir d'un enregistrement de la table LIEU
     * @param array $enreg
     * @return Lieu
     */
    protected static function enregVersMetier(array $enreg) {
        $idLieu = $enreg['IDLIEU'];
        $nomL = $enreg['NOML'];
        $capL = $enreg['CAPL'];
        $adresseL = $enreg['ADRESSEL'];
        $unLieu = new Lieux($idLieu, $nomL, $capL, $adresseL);

        return $unLieu;
    }
 /**
     * Valorise les paramètres d'une requête préparée avec l'état d'un objet Lieux
     * @param Lieux $objetMetier un lieu
     * @param PDOStatement $stmt requête préparée
     */
    protected static function metierVersEnreg(Lieux $objetMetier, PDOStatement $stmt) {
        // On utilise bindValue plutôt que bindParam pour éviter des variables intermédiaires
        // Note : bindParam requiert une référence de variable en paramètre n°2 ; 
        // avec bindParam, la valeur affectée à la requête évoluerait avec celle de la variable sans
        // qu'il soit besoin de refaire un appel explicite à bindParam
        $stmt->bindValue(':idLieu', $objetMetier->getIdLieu());
        $stmt->bindValue(':nomL', $objetMetier->getNomL());
                $stmt->bindValue(':capL', $objetMetier->getCapL());
        $stmt->bindValue(':adresseL', $objetMetier->getAdresseL());
    }

   /**
     * Retourne la liste de tous les lieux
     * @return array tableau d'objets de type Lieu
     */
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM Lieu ORDER BY NOML";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            // Tant qu'il y a des enregistrements dans la table
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                //ajoute un nouveau lieu au tableau
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }

    /**
     * Recherche un lieu selon la valeur de son identifiant
     * @param string $idLieu
     * @return Lieux le lieu trouvé ; null sinon
     */
    public static function getOneById($idLieu) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Lieu WHERE IDLIEU = :idLieu";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':idLieu', $idLieu);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }


   /**
     * Insérer un nouvel enregistrement dans la table à partir de l'état d'un objet métier
     * @param Lieux $objet objet métier à insérer
     * @return boolean =FALSE si l'opération échoue
     */
    public static function insert(Lieux $objet) {
        $requete = "INSERT INTO Lieu VALUES (:idLieu, :nomL,:capL, :adresseL)";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }

    /**
     * Mettre à jour enregistrement dans la table à partir de l'état d'un objet métier
     * @param string identifiant de l'enregistrement à mettre à jour
     * @param Lieux $objet objet métier à mettre à jour
     * @return boolean =FALSE si l'opérationn échoue
     */
    public static function update($idLieu, Lieux $objet) {
        $ok = false;
        $requete = "UPDATE Lieu SET NOML=:nomL, CAPL=:capL, ADRESSEL=:adresseL
                    WHERE IDLIEU=:idLieu";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $stmt->bindParam(':idLieu', $idLieu);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }

    
   /**
     * Détruire un enregistrement de la table LIEU d'après son identifiant
     * @param string identifiant de l'enregistrement à détruire
     * @return boolean =TRUE si l'enregistrement est détruit, =FALSE si l'opération échoue
     */
    public static function delete($idLieu) {
        $ok = false;
        $requete = "DELETE FROM Lieu WHERE IDLIEU = :idLieu";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':idLieu', $idLieu);
        $ok = $stmt->execute();
        $ok = $ok && ($stmt->rowCount() > 0);
        return $ok;
    }

   /**
     * Permet de vérifier s'il existe ou non un lieu ayant déjà le même identifiant dans la BD
     * @param string $idLieu identifiant du lieu à tester
     * @return boolean =true si l'id existe déjà, =false sinon
     */
    public static function isAnExistingId($idLieu) {
        $requete = "SELECT COUNT(*) FROM Lieu WHERE IDLIEU=:idLieu";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':idLieu', $idLieu);
        $stmt->execute();
        return $stmt->fetchColumn(0);
    }
    
    /**
     * Permet de vérifier s'il existe ou non un lieu portant déjà le même nom dans la BD
     * En mode modification, l'enregistrement en cours de modification est bien entendu exclu du test
     * @param boolean $estModeCreation =true si le test est fait en mode création, =false en mode modification
     * @param string $idLieu identifiant du lieu à tester
     * @param string $nomL nom du lieu à tester
     * @return boolean =true si le nom existe déjà, =false sinon
     */
    public static function isAnExistingName($estModeCreation, $idLieu, $nomL) {
        $nomL = str_replace("'", "''", $nomL);
        // S'il s'agit d'une création, on vérifie juste la non existence du nom sinon
        // on vérifie la non existence d'un autre lieu (id!='$id') portant 
        // le même nom
        if ($estModeCreation) {
            $requete = "SELECT COUNT(*) FROM Lieu WHERE NOML=:nomL";
            $stmt = Bdd::getPdo()->prepare($requete);
            $stmt->bindParam(':nomL', $nomL);
            $stmt->execute();
        } else {
            $requete = "SELECT COUNT(*) FROM Lieu WHERE NOML=:nomL AND IDLIEU<>:idLieu";
            $stmt = Bdd::getPdo()->prepare($requete);
            $stmt->bindParam(':idLieu', $idLieu);
            $stmt->bindParam(':nomL', $nomL);
            $stmt->execute();
        }
        return $stmt->fetchColumn(0);
    }
}

