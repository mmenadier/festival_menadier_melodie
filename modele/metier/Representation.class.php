<?php
namespace modele\metier;

/**
 * Description d'une representation
 * 
 * @author melodie
 */
class Representation {
    /** id de la representation
     * @var string
     */
    private $idRepresentation;
    
    /** groupe concerné par la représentation
     * @var modele\metier\Groupe
     */
    private $idGroupe;
    
    /**
     * lieu de la représentation
     * @var modele\metier\Lieux
     */
    private $idLieu;
    
    /**
     * date de la représentation
     * @var string
     */
    private $date;
    
    /**
     * heure de début de la représentation
     * @var string
     */
    private $hDeb;
            
    /**
     * heure de fin de la représentation
     * @var string
     */
    private $hFin;
    
    function __construct($idRepresentation, Groupe $idGroupe, Lieux $idLieu, $date, $hDeb, $hFin) {
        $this->idRepresentation = $idRepresentation;
        $this->idGroupe = $idGroupe;
        $this->idLieu = $idLieu;
        $this->date = $date;
        $this->hDeb = $hDeb;
        $this->hFin = $hFin;
    }

    function getIdRepresentation(): string {
        return $this->idRepresentation;
    }

    function getIdGroupe(): Groupe {
        return $this->idGroupe;
    }

    function getIdLieu(): Lieux {
        return $this->idLieu;
    }

    function getDate(): string {
        return $this->date;
    }

    function getHDeb(): string {
        return $this->hDeb;
    }

    function getHFin(): string {
        return $this->hFin;
    }

    function setIdRepresentation(string $idRepresentation) {
        $this->idRepresentation = $idRepresentation;
    }

    function setIdGroupe(Groupe $idGroupe) {
        $this->idGroupe = $idGroupe;
    }

    function setIdLieu(Lieux $idLieu) {
        $this->idLieu = $idLieu;
    }

    function setDate(string $date) {
        $this->date = $date;
    }

    function setHDeb(string $hDeb) {
        $this->hDeb = $hDeb;
    }

    function setHFin(string $hFin) {
        $this->hFin = $hFin;
    }



    
}
