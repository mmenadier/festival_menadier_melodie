<?php
namespace modele\metier;

/**
 * Description of Groupe
 * un groupe musical se produisant au festival
 * @author eleve
 */
class Lieux {
    
   /** identification du lieu
     * @var string
     */
    private $idLieu;
    
    /** nom de la representation
     * @var string
     */
    private $nomL;
    
    /** capacité d'accueil de la représentation
     * @var string
     */
    private $capL;
    
    /** adresse du lieu de la représentation
     * @var string
     */
    private $adresseL;
    
    function __construct(string $idLieu, string $nomL, string $capL, string $adresseL) {
        $this->idLieu = $idLieu;
        $this->nomL = $nomL;
        $this->capL = $capL;
        $this->adresseL = $adresseL;
    }
    function getIdLieu(): string {
        return $this->idLieu;
    }

    function getNomL(): string {
        return $this->nomL;
    }

    function getCapL(): string {
        return $this->capL;
    }

    function getAdresseL(): string {
        return $this->adresseL;
    }

    function setIdLieu(string $idLieu) {
        $this->idLieu = $idLieu;
    }

    function setNomL(string $nomL) {
        $this->nomL = $nomL;
    }

    function setCapL(string $capL) {
        $this->capL = $capL;
    }

    function setAdresseL(string $adresseL) {
        $this->adresseL = $adresseL;
    }


    
}
?>