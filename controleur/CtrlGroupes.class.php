<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace controleur;

use controleur\GestionErreurs;
use modele\dao\GroupeDAO;
use modele\dao\AttributionDAO;
use modele\metier\Groupe;
use modele\dao\Bdd;
use vue\groupes\vueListeGroupes;
use vue\groupes\VueDetailGroupes;
use vue\groupes\VueSaisieGroupes;
use vue\groupes\VueSupprimerGroupes;

class CtrlGroupes extends ControleurGenerique {

    /** controleur= groupes & action= defaut
     * Afficher la liste des groupes      */
    public function defaut() {
        $this->liste();
    }

    /** controleur= groupes & action= liste
     * Afficher la liste des groupes      */
    public function liste() {
        $laVue = new vueListeGroupes();
        $this->vue = $laVue;
        // On récupère un tableau composé de la liste des groupes  
        Bdd::connecter();
        $laVue->setLesGroupesAvecNbAttributions($this->getTabGroupesAvecNbAttributions());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupes");
        $this->vue->afficher();
    }
    
            /** controleur= Groupes & action=detail & id=groupes
     * Afficher un établissement d'après son identifiant     */
public function detail() {
        $idGroupes = $_GET["id"];
        $this->vue = new VueDetailGroupes();
        // Lire dans la BDD les données du groupe à afficher
        Bdd::connecter();
        $this->vue->setUnGroupe(GroupeDAO::getOneById($idGroupes));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupes");
        $this->vue->afficher();
    }
    
/** controleur= groupes & action=creer
     * Afficher le formulaire d'ajout d'un groupe     */
    public function creer() {
        $laVue = new VueSaisieGroupes();
        $this->vue = $laVue;
        $laVue->setActionRecue("creer");
        $laVue->setActionAEnvoyer("validerCreer");
        $laVue->setMessage("Nouveau groupe");
        // En création, on affiche un formulaire vide
        /* @var Groupe $unGroupe */
        $unGroupe = new Groupe("", "", "", "", "", "","");
        $laVue->setUnGroupe($unGroupe);
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupes");
        $this->vue->afficher();
    }

    /** controleur= groupes & action=validerCreer
     * ajouter d'un groupe dans la base de données d'après la saisie    */
    public function validerCreer() {
        Bdd::connecter();
        /* @var Groupe $unGroupe  : récupération du contenu du formulaire et instanciation d'un groupe */
        $unGroupe = new Groupe($_REQUEST['id'],$_REQUEST['nom'],  $_REQUEST['identite'], $_REQUEST['adresse'], $_REQUEST['nbPersonnes'], $_REQUEST['nomPays'], $_REQUEST['hebergement']);
        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de création (paramètre n°1 = true)
        $this->verifierDonneesGroupe($unGroupe, true);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer le groupe
            GroupeDAO::insert($unGroupe);
            // revenir à la liste des groupes
            header("Location: index.php?controleur=groupes&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de création
            $laVue = new VueSaisieGroupes();
            $this->vue = $laVue;
            $laVue->setActionRecue("creer");
            $laVue->setActionAEnvoyer("validerCreer");
            $laVue->setMessage("Nouveau groupe");
            $laVue->setUnEtablissement($unGroupe);
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - groupe");
            $this->vue->afficher();
        }
    }
    

    
    /** controleur= Groupes & action=modifier $ id=Groupes du groupe à modifier
     * Afficher le formulaire de modification d'un groupe     */
    public function modifier() {
        $idGroupes = $_GET["id"];
        $laVue = new VueSaisieGroupes();
        $this->vue = $laVue;
        // Lire dans la BDD les données de l'établissement à modifier
        Bdd::connecter();
        /* @var Groupes $Groupes */
        $leGroupes = GroupeDAO::getOneById($idGroupes);
        $this->vue->setUnGroupe($leGroupes);
        $laVue->setActionRecue("modifier");
        $laVue->setActionAEnvoyer("validerModifier");
        $laVue->setMessage("Modifier le groupe : " . $leGroupes->getNom() . " (" . $leGroupes->getId() . ")");
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Groupes");
        $this->vue->afficher();
    }

    /** controleur= Groupes & action=validerModifier
     * modifier un groupe dans la base de données d'après la saisie    */
    public function validerModifier() {
        Bdd::connecter();
        /* @var Groupes $unGroupe  : récupération du contenu du formulaire et instanciation d'un groupe */
        $unGroupe = new Groupe($_REQUEST['id'],$_REQUEST['nom'],  $_REQUEST['identite'], $_REQUEST['adresse'], $_REQUEST['nbPersonnes'], $_REQUEST['nomPays'], $_REQUEST['hebergement']);

        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de modification (paramètre n°1 = false)
        $this->verifierDonneesGroupe($unGroupe, false);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer les modifications pour le groupe
            GroupeDAO::update($unGroupe->getId(), $unGroupe);
            // revenir à la liste des groupes
            header("Location: index.php?controleur=groupes&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de modification
            $laVue = new VueSaisieGroupes();
            $this->vue = $laVue;
            $laVue->setUnGroupe($unGroupe);
            $laVue->setActionRecue("modifier");
            $laVue->setActionAEnvoyer("validerModifier");
            $laVue->setMessage("Modifier un groupe : " . $unGroupe->getNom() . " (" . $unGroupe->getId() . ")");
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - Groupes");
            $this->vue->afficher();
        }
    }

     /** controleur= groupes & action=supprimer & id=identifiant_groupe
     * Supprimer un groupe d'après son identifiant     */
    public function supprimer() {
        $idGroupe = $_GET["id"];
        $this->vue = new VueSupprimerGroupes();
        // Lire dans la BDD les données du groupe à supprimer
        Bdd::connecter();
        $this->vue->setUnGroupe(GroupeDAO::getOneById($idGroupe));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupes");
        $this->vue->afficher();
    }

    /** controleur= groupes & action= validerSupprimer
     * supprimer un groupe dans la base de données après confirmation   */
    public function validerSupprimer() {
        Bdd::connecter();
        if (!isset($_GET["id"])) {
            // pas d'identifiant fourni
            GestionErreurs::ajouter("Il manque l'identifiant du groupe à supprimer");
        } else {
            // suppression de l'établissement d'après son identifiant
            GroupeDAO::delete($_GET["id"]);
        }
        // retour à la liste des établissements
        header("Location: index.php?controleur=groupes&action=liste");
    }

            private function verifierDonneesGroupe(Groupe $unGroupe, bool $creation) {
        // Vérification des champs obligatoires.
        // Dans le cas d'une création, on vérifie aussi l'id
        if (($creation && $unGroupe->getId() == "") || $unGroupe->getNom() == "" || $unGroupe->getIdentite() == "" || $unGroupe->getAdresse() == "" ||
                $unGroupe->getNbPers() == "" || $unGroupe->getNomPays() == "" || $unGroupe->getHebergement() == "") {
            GestionErreurs::ajouter('Chaque champ suivi du caractère * est obligatoire');
        }
        // En cas de création, vérification du format de l'id et de sa non existence
        if ($creation && $unGroupe->getId() != "") {
            // Si l'id est constitué d'autres caractères que de lettres non accentuées 
            // et de chiffres, une erreur est générée
            if (!estAlphaNumerique($unGroupe->getId())) {
                GestionErreurs::ajouter("L'identifiant doit comporter uniquement des lettres non accentuées et des chiffres");
            } else {
                if (GroupeDAO::isAnExistingId($unGroupe->getId())) {
                    GestionErreurs::ajouter("Le groupe " . $unGroupe->getId() . " existe déjà");
                }
            }
        }
    }
     public function getTabGroupesAvecNbAttributions(): Array {
        $lesGroupesAvecNbAttrib = Array();
        $lesGroupes = GroupeDAO::getAll();
        foreach ($lesGroupes as $unGroupe) {
            /* @var Groupe $unGroupe */
            $lesGroupesAvecNbAttrib[$unGroupe->getId()]['groupe'] = $unGroupe;
            //$lesGroupesAvecNbAttrib[$unGroupe->getId()]['nbGroupe'] = count(AttributionDAO::getAllByIdGroup($unGroupe->getId()));
        }
        return $lesGroupesAvecNbAttrib;
    }


}
