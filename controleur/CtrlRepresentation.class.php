<?php

/**
 * Contrôleur de gestion des representations
 * @author Melodie
 * @version 2020
 */

namespace controleur;


use modele\dao\RepresentationDAO;
use modele\dao\Bdd;
use vue\representations\VueConsultationRepresentations;
use vue\representations\VueDetailRepresentations;
use vue\representations\VueSaisieRepresentations;
use modele\dao\AttributionDAO;
use modele\dao\GroupeDAO;
use modele\dao\LieuxDAO;
use modele\metier\Representation;
use modele\metier\Groupe;
use modele\metier\Lieux;
use vue\representation\VueConsultationRepresentation;
use vue\representation\VueSaisieRepresentation;
use vue\representation\VueSupprimerRepresentation;



class CtrlRepresentation extends ControleurGenerique {
    public function defaut() {
        $this->consulter();
    }
    public function consulter() {
        $laVue = new VueConsultationRepresentations();
        $this->vue = $laVue;
        // On récupère un tableau composé de la liste des representations
        Bdd::connecter();
        $laVue->setLesRepresentations(RepresentationDAO::getAll());


        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representations");
        $this->vue->afficher();
    }
   
    /** controleur= representation & action=supprimer & id=identifiant_representation
     * Supprimer une representation d'après son identifiant     */
    public function supprimer() {
        $idRepresentation = $_GET["id"];
        $this->vue = new \vue\representations\VueSupprimerRepresentations();
        // Lire dans la BDD les données des representations à supprimer
        Bdd::connecter();
        $this->vue->setUneRepresentation(RepresentationDAO::getOneById($idRepresentation));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representation");
        $this->vue->afficher();
    }

    /** controleur= representation & action= validerSupprimer
     * supprimer une representation dans la base de données après confirmation   */
    public function validerSupprimer() {
        Bdd::connecter();
        if (!isset($_GET["id"])) {
            // pas d'identifiant fourni
            GestionErreurs::ajouter("Il manque l'identifiant de la representation à supprimer");
        } else {
            // suppression de la representation d'après son identifiant
            RepresentationDAO::delete($_GET["id"]);
        }
        // retour à la liste des representations
        header("Location: index.php?controleur=representation&action=consulter");
    }
    
    
            /** controleur= representation & action=detail & id=representation
     * Afficher une représentation d'après son identifiant     */
        public function detail() {
        $idRepresentation = $_GET["id"];
        $this->vue = new VueDetailRepresentations();
        // Lire dans la BDD les données de la représentation à afficher
        Bdd::connecter();
        $this->vue->setUneRepresentation(RepresentationDAO::getOneById($idRepresentation));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representations");
        $this->vue->afficher();
    }
    
    
    /** controleur= representation & action=creer
     * Afficher le formulaire d'ajout d'une representation     */
    public function creer() {
        $laVue = new VueSaisieRepresentations();
        $this->vue = $laVue;
        $laVue->setActionRecue("creer");
        $laVue->setActionAEnvoyer("validerCreer");
        $laVue->setMessage("Nouvelle representation");
        // En création, on affiche un formulaire vide
        $idLieu = new Lieux("", "", "", "");
        $idGroupe = new Groupe("", "", "", "", "", "", "");
        /* @var Representation $uneRepresentation */
        $uneRepresentation = new Representation("", $idGroupe, $idLieu, "", "", "");
        $laVue->setUneRepresentation($uneRepresentation);
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representations");
        $this->vue->afficher();

    }

    /** controleur= representation & action=validerCreer
     * ajouter d'un groupe dans la base de données d'après la saisie    */
    public function validerCreer() {
        Bdd::connecter();
        /* @var Representation $uneRepresentation  : récupération du contenu du formulaire et instanciation d'une representation */
        $uneRepresentation = new Representation($_REQUEST['id'], GroupeDAO::getOneById($_REQUEST['groupe']), LieuxDAO::getOneById($_REQUEST['lieu']), $_REQUEST['date'], $_REQUEST['hDeb'], $_REQUEST['hFin']);
        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de création (paramètre n°1 = true)
        $this->verifierDonneesRepresentations($uneRepresentation, true);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer le groupe
            RepresentationDAO::insert($uneRepresentation);
            // revenir à la liste des representations
            header("Location: index.php?controleur=representation&action=consulter");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de création
            $laVue = new VueSaisieRepresentations();
            $this->vue = $laVue;
            $laVue->setActionRecue("creer");
            $laVue->setActionAEnvoyer("validerCreer");
            $laVue->setMessage("Nouvelle Representation");
           
            $laVue->setUneRepresentation($uneRepresentation);
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - representation");
            $this->vue->afficher();
        }
    }
    
    /** controleur= representation & action=modifier $ id=identifiant d'une Representation à modifier
     * Afficher le formulaire de modification d'une Representation     */
    public function modifier() {
        $idRepresentation = $_GET["id"];
        $laVue = new VueSaisieRepresentations();
        $this->vue = $laVue;
        // Lire dans la BDD les données du Groupe à modifier
        Bdd::connecter();
        /* @var Groupe $leGroupe */
        $laRepresentation = RepresentationDAO::getOneById($idRepresentation);
        $this->vue->setUneRepresentation($laRepresentation);
        
        $laVue->setActionRecue("modifier");
        $laVue->setActionAEnvoyer("validerModifier");
        $laVue->setMessage("Modifier la representation : " . $laRepresentation->getIdRepresentation() .".");
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representation");
        $this->vue->afficher();
    }

    /** controleur= representation & action=validerModifier
     * modifier une representation dans la base de données d'après la saisie    */
    public function validerModifier() {
        Bdd::connecter();
        /* @var Representation $uneRepresentation  : récupération du contenu du formulaire et instanciation d'une Representation */
        $uneRepresentation = new Representation($_REQUEST['id'], GroupeDAO::getOneById($_REQUEST['groupe']),LieuxDAO::getOneById($_REQUEST['lieu']), $_REQUEST['date'], $_REQUEST['hDeb'], $_REQUEST['hFin']);
        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de modification (paramètre n°1 = false)
        $this->verifierDonneesRepresentations($uneRepresentation, false);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer les modifications pour un groupe
            RepresentationDao::update($uneRepresentation->getIdRepresentation(), $uneRepresentation);
            // revenir à la liste des representations
            header("Location: index.php?controleur=representation&action=consulter");
        } else {
            // s'il y a des erreurs,
            // réafficher le formulaire de modification
            $laVue = new VueSaisieRepresentation();
            $this->vue = $laVue;
            $laVue->setUneRepresentation($uneRepresentation);
            $lesGroupes = GroupeDAO::getAll();
            $this->vue->setLesGroupes($lesGroupes);
            $lesLieux = LieuxDAO::getAll();
            $this->vue->setLesLieux($lesLieux);
            $laVue->setActionRecue("modifier");
            $laVue->setActionAEnvoyer("validerModifier");
            $laVue->setMessage("Modifier la Représentation : " . $uneRep->getIdRepresentation());
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - Representation");
            $laVue->setVersion($this->version);
            $this->vue->afficher();
        }
    }
    
    
    
    
    
                private function verifierDonneesRepresentations(representation $uneRepresentation, bool $creation) {
        // Vérification des champs obligatoires.
        // Dans le cas d'une création, on vérifie aussi l'id
        if (($creation && $uneRepresentation->getIdRepresentation() == "") || $uneRepresentation->getIdLieu()->getNomL() == "" || $uneRepresentation->getIdGroupe()->getNom() == "" 
                || $uneRepresentation->getDate() == "" || $uneRepresentation->getHDeb() == "" || $uneRepresentation->getHFin() == "") {
            GestionErreurs::ajouter('Chaque champ suivi du caractère * est obligatoire');
        }
        // En cas de création, vérification du format de l'id et de sa non existence
        if ($creation && $uneRepresentation->getIdRepresentation() != "") {
            // Si l'id est constitué d'autres caractères que de lettres non accentuées 
            // et de chiffres, une erreur est générée
            if (!estAlphaNumerique($uneRepresentation->getIdRepresentation())) {
                GestionErreurs::ajouter("L'identifiant doit comporter uniquement des lettres non accentuées et des chiffres");
            } else {
                if (RepresentationDAO::isAnExistingId($uneRepresentation->getIdRepresentation())) {
                    GestionErreurs::ajouter("La representation " . $uneRepresentation->getIdRepresentation() . " existe déjà");
                }
            }
        }
    }
     public function getTabRepresentationsAvecNbAttributions(): Array {
        $lesRepresentationsAvecNbAttrib = Array();
        $lesRepresentations = RepresentationDAO::getAll();
        foreach ($lesRepresentations as $uneRepresentation) {
            /* @var Representation $uneRepresentation */
            $lesRepresentationsAvecNbAttrib[$uneRepresentation->getIdRepresentation()]['representation'] = $uneRepresentation;
                    }
        return $lesRepresentationsAvecNbAttrib;
    
        
        
        }
}
