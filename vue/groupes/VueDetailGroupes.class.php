<?php

namespace vue\groupes;

use vue\vueListeGroupes;
//use modele\metier\Etablissement;

/**
 * Description Consultatation du formulaire.
 * @author Guyon Menadier Dabin
 * @version 2018
 */
class VueDetailGroupes extends \vue\groupes\vueListeGroupes {

    /** @var Groupe identificateur d'un Groupe à afficher */
    private $unGroupe;

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();

        ?>
        <br>
        <table width='60%' cellspacing='0' cellpadding='0' class='tabNonQuadrille'> 
            <tr class='enTeteTabNonQuad'>
                <td colspan='3'><strong><?= $this->unGroupe->getNom() ?></strong></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td  width='20%'> Id : </td>
                <td><?= $this->unGroupe->getId() ?></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Identité du responsable : </td>
                <td><?= $this->unGroupe->getIdentite() ?></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Adresse Postal : </td>
                <td><?= $this->unGroupe->getAdresse() ?></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Nombre de personnes : </td>
                <td><?= $this->unGroupe->getNbPers() ?></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Pays : </td>
                <td><?= $this->unGroupe->getNomPays() ?></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Hébergement (O/N) : </td>
                <td><?= $this->unGroupe->getHebergement() ?></td>
            </tr>
        </table>
        <br>
        <a href='index.php?controleur=Groupes&action=liste'>Retour</a>
        <?php
        include $this->getPied();
    }

    function setUnGroupe(\modele\metier\Groupe $unGroupe) {
        $this->unGroupe = $unGroupe;
    }


}
