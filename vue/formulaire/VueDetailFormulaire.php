<?php

namespace vue\formulaire;

use vue\VueGenerique;
//use modele\metier\Etablissement;

/**
 * Description Page de consultation d'un établissement donné
 * @author prof
 * @version 2018
 */
class VueDetailFormulaire extends VueGenerique {

    /** @var Etablissement identificateur de l'établissement à afficher */
    private $unFormulaire;

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();

        ?>
        <br>
        <table width='60%' cellspacing='0' cellpadding='0' class='tabNonQuadrille'> 
            <tr class='enTeteTabNonQuad'>
                <td colspan='3'><strong><?= $this->unFormulaire->getNom() ?></strong></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td  width='20%'> Id: </td>
                <td><?= $this->unFormulaire->getId() ?></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Adresse: </td>
                <td><?= $this->unFormulaire->getAdresse() ?></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Code postal: </td>
                <td><?= $this->unFormulaire->getCdp() ?></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Ville: </td>
                <td><?= $this->unFormulaire->getVille() ?></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Téléphone: </td>
                <td><?= $this->unFormulaire->getTel() ?></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> E-mail: </td>
                <td><?= $this->unFormulaire->getEmail() ?></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Type: </td>
        <?php
        if ($this->unFormulaire->getTypeEtab() == 1) {
            ?>
                    <td> formulaire </td>
                    <?php
                } else {
                    ?>
                    <td> Autre formulaire </td>
                    <?php
                }
                ?>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Responsable: </td>
                <td><?= $this->unFormulaire->getCiviliteResp() ?>&nbsp; <?= $this->unFormulaire->getNomResp() ?>&nbsp; <?= $this->unFormulaire->getPrenomResp() ?>
                </td>
            </tr> 
        </table>
        <br>
        <a href='index.php?controleur=etablissements&action=liste'>Retour</a>
        <?php
        include $this->getPied();
    }

    function setunFormulaire(Formulaire $unFormulaire) {
        $this->unFormulaire = $unFormulaire;
    }


}
