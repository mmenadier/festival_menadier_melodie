<?php

namespace vue\representations;

use vue\VueGenerique;
use modele\metier\Representation;

/**
 * Page de suppression d'une representation donnée
 * @author Melodie
 * @version 2020
 */
class VueSupprimerRepresentations extends VueGenerique {

    /** @var Representation identificateur de la representation à afficher */
    private $uneRepresentation;

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
<br><center>Voulez-vous vraiment supprimer la representation : <?= $this->uneRepresentation->getIdRepresentation() ?> ?
            <h3><br>
                <a href="index.php?controleur=representation&action=validerSupprimer&id=<?= $this->uneRepresentation->getIdRepresentation() ?>">Oui</a>
                &nbsp; &nbsp; &nbsp; &nbsp;
                <a href="index.php?controleur=representation">Non</a></h3>
        </center>
        <?php
        include $this->getPied();
    }

    function setUneRepresentation(Representation $uneRepresentation) {
        $this->uneRepresentation = $uneRepresentation;
    }

}

