<?php

/**
 * Description Page de consultation de la liste des représentations
 * -> affiche un tableau constitué d'une ligne d'entête et d'une ligne par représentation
 * @author Melodie
 * @version 2020
 */

namespace vue\representations;

use vue\VueGenerique;




class VueConsultationRepresentations extends VueGenerique {

    /** @var array liste des Representations */
    private $lesRepresentations;
    

    public function __construct() {
        parent::__construct();
    }
    
    public function afficher() {
     include $this->getEntete();
     
?>

<h2>Les représentations</h2>
<br>
<table width="50%" cellspacing="0" cellpadding="0" class="tabQuadrille">
<?php
            $DateTest="0000-00-00";
            foreach ($this->lesRepresentations as $uneRepresentation) {         
                     $uneDate= $uneRepresentation->getDate();
                     $id = $uneRepresentation->getIdRepresentation();
                     
                     if ($DateTest!=$uneDate){
                         $DateTest=$uneDate ;
                ?>
</table>
<h3><?= $uneDate?></h3>
            

<table width="50%" cellspacing="0" cellpadding="0" class="tabQuadrille">
            </tr>
                    
                     <tr class="enTeteTabQuad">
                        <td width="30%">Lieu</td>
                        <td width="30%">Groupe</td> 
                        <td width="30%">Heure Début</td> 
                        <td width="30%">Heure Fin</td> 
                        <td width="60%"></td> 
                        <td width="60%"></td> 
                        <td width="60%"></td>
                    </tr>
                        <tr class="ligneTabQuad">
                        <td ><?=$uneRepresentation->getIdLieu()->getNomL()?></td>
                        <td><?=$uneRepresentation->getIdGroupe()->getNom()?></td> 
                        <td><?=$uneRepresentation->getHDeb()?></td> 
                        <td><?=$uneRepresentation->getHFin()?></td> 
                        <td><a href="index.php?controleur=representation&action=detail&id=<?= $id ?>" > Detail </a></td>
                        <td><a href="index.php?controleur=representation&action=modifier&id=<?= $id ?>" > Modifier </a></td>
                        <td><a href="index.php?controleur=representation&action=supprimer&id=<?= $id ?>" > Supprimer </a></td>
                    </tr>

                    
        <br>
 <?php 
            }else{
                
            ?>
                
                        <tr class="ligneTabQuad">
                        <td ><?=$uneRepresentation->getIdLieu()->getNomL()?></td>
                        <td><?=$uneRepresentation->getIdGroupe()->getNom()?></td>
                        <td><?=$uneRepresentation->getHDeb()?></td> 
                        <td><?=$uneRepresentation->getHFin()?></td> 
                        <td><a href="index.php?controleur=representation&action=detail&id=<?= $id ?>" > Detail </a></td>
                        <td><a href="index.php?controleur=representation&action=modifier&id=<?= $id ?>" > Modifier </a></td>
                        <td><a href="index.php?controleur=representation&action=supprimer&id=<?= $id ?>" > Supprimer </a></td>
                    </tr>
                    
<?php
            }
            }
            ?>
        </table>
        <br>
        <a href="index.php?controleur=representation&action=creer" > Ajout d'une representation </a >
      
 <?php
        include $this->getPied();
    }

    function setLesRepresentations($lesRepresentations) {
        $this->lesRepresentations = $lesRepresentations;
    }

}
    
 