<?php

namespace vue\representations;


use vue\VueGenerique;


/**
 * Description Consultatation du formulaire.
 * @author Melodie
 * @version 2020
 */
class VueDetailRepresentations extends VueGenerique {

    /** @var Representation identificateur d'une Representations à afficher */
    private $uneRepresentation;

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();

        ?>
        <br>
        <table width='60%' cellspacing='0' cellpadding='0' class='tabNonQuadrille'> 
            <tr class='enTeteTabNonQuad'>
                <td colspan='3'><strong><?= $this->uneRepresentation->getIdRepresentation() ?></strong></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Lieu : </td>
                <td><?= $this->uneRepresentation->getIdLieu()->getNomL() ?></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Groupe : </td>
                <td><?= $this->uneRepresentation->getIdGroupe()->getNom() ?></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Date : </td>
                <td><?= $this->uneRepresentation->getDate() ?></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Heure Début : </td>
                <td><?= $this->uneRepresentation->getHDeb() ?></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td> Heure Fin : </td>
                <td><?= $this->uneRepresentation->getHFin() ?></td>
            </tr>
        </table>
        <br>
        <a href='index.php?controleur=representation&action=consulter'>Retour</a>
        <?php
        include $this->getPied();
    }

    function setUneRepresentation(\modele\metier\Representation $uneRepresentation) {
        $this->uneRepresentation = $uneRepresentation;
    }


}

